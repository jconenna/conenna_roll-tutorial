﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour
{
    Vector3 startPos; //where to start
    public float speed = 1f;// speed of pickup

    private void Start()
    {
        startPos = transform.position; // saving the starting position
    }
    void Update()
    {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);

        float length = 3f; //distance of pickups
        Vector3 direction = Vector3.right; //moving the pickups left to right

        //Move the pick up using ping pong 
        transform.position = startPos + (direction * (Mathf.PingPong(Time.time * speed, length) - length / 2f));
    }
}