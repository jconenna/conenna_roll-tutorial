﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement; //scene managing code in script

public class PlayerController : MonoBehaviour
{

    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;

    float timePlayed; //how much time the player has been playing
    bool gameFinished; // if the player has won the game or not

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";

    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 12)
        {
            gameFinished = true; //recored the player had collected everything

            winText.text = "You Win!";

            Invoke("RestartLevel", 2f); // Level restarts after 2 seconds
        }
    }
    void RestartLevel()
    {
        // reload the current level
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    // update is called once per game
    private void Update()
    {
        if (!gameFinished) //game is still being played
        {
            timePlayed += Time.deltaTime; // adding the amount of time per frame
            print("Time Played:" + timePlayed); // printing to console

        }
    }

}
